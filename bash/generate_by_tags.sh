#!/bin/bash

cd $(dirname "$0")

# rm -rf ../public/* # TODO remove/comment before git commit

git tag --list > tags.txt
echo main >> tags.txt # TODO change main to master before using with QuNex repo

# for each line in tags_to_ignore.txt remove entries from tags.txt
for tag in $(cat tags_to_ignore.txt)
do
  sed -i "/$tag/d" tags.txt
done

# build documentation for each tag
for tag in $(cat tags.txt)
do
  git clone --depth=1 --branch=$tag https://gitlab.com/bavmar/sphinx-test.git /tmp/sphinx-temp

  mkdir ../public/$tag -p
  sphinx-build /tmp/sphinx-temp ../public/$tag -a

  rm -rf /tmp/sphinx-temp
done

rm tags.txt

echo "Documentation for all tags (including master) has been generated."
