# Readme
Repo for testing documentation hosting platforms.

Currently being tested on:
* [bavmar.gitlab.io/sphinx-test](https://bavmar.gitlab.io/sphinx-test/main/)
* [sphinx-test-for-rtd.readthedocs.io](https://sphinx-test-for-rtd.readthedocs.io/)
