.. MyCode documentation master file, created by
   sphinx-quickstart on Wed Mar 16 09:46:56 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MyCode's documentation!
==================================
**This is the brand new intro section, introduced with 0.4!**

Additional text from 0.5.

.. automodule:: example_numpy
   :members:

.. automodule:: example
   :members:

.. automodule:: example_google
   :members:

.. automodule:: test_google
   :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
